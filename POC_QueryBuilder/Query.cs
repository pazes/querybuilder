﻿namespace POC_QueryBuilder
{
    public class Query
    {
        public int IdQuery { get; set; }
        public int Priority { get; set; }
        public int Processor { get; set; }
    }
}
