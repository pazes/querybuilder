﻿using CGP.Switch.Core.Shared.EntryPoint.Requests;
using Newtonsoft.Json.Linq;
using POC_QueryBuilder.Specification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace POC_QueryBuilder
{
    public class Program
    {
        private static void Main(string[] args)
        {
            // Generic - Test
            System.Diagnostics.Stopwatch timer = System.Diagnostics.Stopwatch.StartNew();
            timer.Start();
            string json = @"{""Fuc"":""11111111"",""TerminalId"":""121231221""}";
            JObject person = JObject.Parse(json);
            JObject[] people = new[] { person };
            bool isMatch = people.Any(p => p.GetValue("Fuc").Value<string>() == "11111111");
            timer.Stop();
            Console.WriteLine(isMatch ? "Found in " + timer.ElapsedMilliseconds.ToString() + " miliseconds" : "not found");
            timer.Reset();


            // Request - Test 
            TRReqRoot req = JsonConvert.DeserializeObject<TRReqRoot>(File.ReadAllText(@"json\Sale.json"));
            List<TRReq> requests = new List<TRReq>();
            requests.Add(req.TRREQ);
            timer.Start();

            timer.Stop();
            Console.WriteLine("With Request" + (isMatch ? " Found in " + timer.ElapsedMilliseconds.ToString() + " miliseconds" : " not found"));


            List<Query> queryList = InitializeQueries();
            List<Policy> policyList = InitializePolicies();
            List<PolicyQuery> policyQueryList = InitializePoliciesQuery();
            timer.Reset();
            timer.Start();

            // Define specifications
            ISpecification<dynamic> merchanIdExpSpec = new ExpressionSpecification<dynamic>(o => o.TerminalTransID.GetMerchantID() == "285337606");
            ISpecification<dynamic> terminalIdExpSpec = new ExpressionSpecification<dynamic>(o => o.TerminalTransID.TerminalID == "00000001");
            ISpecification<dynamic> timestampIdExpSpec = new ExpressionSpecification<dynamic>(o => o.TerminalTransID.TimeStamp == "200121130007");


            // simple validation in request
            var merchantIdok = requests.FindAll(o => merchanIdExpSpec.IsSatisfiedBy(o));
            timer.Stop();
            Console.WriteLine("Simple validation With Request" + (merchantIdok.Any() ? " Found in " + timer.ElapsedMilliseconds.ToString() + " miliseconds" : " not found"));
            timer.Reset();

            // complex validations
            timer.Start();

            ISpecification<dynamic> merchantORterminalANDtimeStampSpec = ((merchanIdExpSpec).Or(terminalIdExpSpec)).And(timestampIdExpSpec);
            var merchantORterminalANDtimeStamp = requests.FindAll(o => merchantORterminalANDtimeStampSpec.IsSatisfiedBy(o));

            timer.Stop();
            Console.WriteLine("Complex validation With Request" + (merchantORterminalANDtimeStamp.Any() ? " Found in " + timer.ElapsedMilliseconds.ToString() + " miliseconds" : " not found"));
            timer.Reset();


            // Complex generic validations
            timer.Start();
            foreach (Query q in queryList.OrderBy(c => c.Priority))
            {
                ISpecification<dynamic> finalValidation = new ExpressionSpecification<dynamic>(o => true);

                foreach (PolicyQuery pq in policyQueryList.Where(o => o.IdQuery == q.IdQuery))
                {
                    Policy p = policyList.First(o => o.IdPolicy == pq.IdPolicy);

                    ISpecification<dynamic> genericSpec;

                    if (!p.InRequest)
                    {
                        // Redis or other datasource evaluation. unable to do generic. All that info will be in OperationContext
                        genericSpec = new ExpressionSpecification<dynamic>(o => "o.OperationContext." + p.Field == pq.Value);

                        // Redis Client
                        //genericSpec = new ExpressionSpecification<dynamic>(o => RedisClient.Get(Valor) == pq.Value);
                    }
                    else
                    {
                        // InReuest values eval                        
                        genericSpec = new ExpressionSpecification<dynamic>(o => p.Field == pq.Value);
                    }

                    switch (pq.LogicOperator)
                    {
                        case "NOT":
                            finalValidation = finalValidation.Not(genericSpec);
                            break;
                        case "AND":
                            finalValidation = finalValidation.And(genericSpec);
                            break;
                        case "OR":
                            finalValidation = finalValidation.Or(genericSpec);
                            break;
                    }
                }

                var complexGenericValidation = requests.FindAll(o => finalValidation.IsSatisfiedBy(o));

                timer.Stop();
                Console.WriteLine("Complex generic validation With Request" + ((complexGenericValidation.Any() ? " Found in: " : " not found in: ") + timer.ElapsedMilliseconds.ToString() + " miliseconds" ));
            }
        }

        #region Initializers
        private static List<Query> InitializeQueries()
        {
            return new List<Query>
            {
                //IF[Adquirente] = X  AND[Canal] = e - commerce THEN processor = GP
                new Query(){IdQuery=1,Priority=1,Processor=1},
                //IF[Marca] = VISA AND[Canal] = F2F AND[País de la tarjeta] <> Spain THEN processor = VISA
                new Query(){IdQuery=2,Priority=2,Processor=2},
                //IF[País del Comercio] IN(USA, CA, SING) AND[Operación] NOT[DCC] THEN processor = REDSYS
                new Query(){IdQuery=3,Priority=3,Processor=3},
                //IF[BIN] NOT IN(123456, 234567, 345678, 456789, 567890, 135790, 432156, 789765, 321234, 678886, 123477)  AND[Adquirente] = X AND[País de la tarjeta] IN(PT, BR) THEN processor = SIBS
                new Query(){IdQuery=4,Priority=4,Processor=4},
            };
        }
        private static List<Policy> InitializePolicies()
        {
            return new List<Policy>
            {
                new Policy{IdPolicy=1,Name="Channel",Field="TR-REQ.TerminalTransID.MerchantID",InRequest=true},
                new Policy{IdPolicy=2,Name="CardBrand",Field="TR-REQ.TransactionData.POSEntryMode.CardDataCaptureMethod",InRequest=true},
                new Policy{IdPolicy=3,Name="RID/PIX",Field="TR-REQ.TerminalTransID.TimeStamp",InRequest=true},
                new Policy{IdPolicy=4,Name="Country",Field="Country",InRequest=false},
                new Policy{IdPolicy=5,Name="Acquirer",Field="Acquirer",InRequest=false},
                new Policy{IdPolicy=6,Name="BIN",Field="TR-REQ.CardData.Bin",InRequest=true},
                new Policy{IdPolicy=7,Name="Operation",Field="Operation",InRequest=false },
                new Policy{IdPolicy=8,Name="Service",Field="Service",InRequest=false }
            };
        }
        private static List<PolicyQuery> InitializePoliciesQuery()
        {
            return new List<PolicyQuery>
            {
                //IF[Adquirente] = X  AND[Canal] = e - commerce THEN processor = GP
                new PolicyQuery{IdQuery=1,IdPolicy=5,Value="285337606",Order=1, LogicOperator="AND"},
                new PolicyQuery{IdQuery=1,IdPolicy=1,Value="M",Order=2,LogicOperator="AND" },

                //IF[Marca] = VISA AND[Canal] = F2F AND[País de la tarjeta] <> Spain THEN processor = VISA
                new PolicyQuery{IdQuery=2,IdPolicy=2,Value="VISA",Order=1,LogicOperator="AND" },
                new PolicyQuery{IdQuery=2,IdPolicy=1,Value="F2F",Order=2,LogicOperator="AND" },
                new PolicyQuery{IdQuery=2,IdPolicy=4,Value="EEUU",Order=3,LogicOperator="NOT" },

                //IF[País del Comercio] IN(USA, CA, SING) AND[Operación] NOT[DCC] THEN processor = REDSYS
                new PolicyQuery{IdQuery=3,IdPolicy=3,Value="Timestamp",Order=1,LogicOperator="AND" },
                new PolicyQuery{IdQuery=3,IdPolicy=4,Value="TransNumber",Order=2,LogicOperator="OR" },

                //IF[BIN] NOT IN(123456, 234567, 345678, 456789, 567890, 135790, 432156, 789765, 321234, 678886, 123477)  
                //AND[Adquirente] = X 
                //AND[País de la tarjeta] IN(PT, BR) THEN processor = SIBS
                new PolicyQuery{IdQuery=4,IdPolicy=6,Value="123456, 234567, 345678, 456789, 567890, 135790, 432156, 789765, 321234, 678886, 123477",Order=1,LogicOperator="AND" },
                new PolicyQuery{IdQuery=4,IdPolicy=4,Value="X",Order=2,LogicOperator="NOT" },
                new PolicyQuery{IdQuery=4,IdPolicy=5,Value="PT, BR",Order=3,LogicOperator="AND" },
            };
        }
        #endregion
    }
}
