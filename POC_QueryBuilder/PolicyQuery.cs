﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POC_QueryBuilder
{
    public class PolicyQuery
    {
        public int IdQuery { get; set; }

        public int IdPolicy { get; set; }

        public string Value { get; set; }

        public int Order { get; set; }
        public string LogicOperator { get; set; }
    }
}
