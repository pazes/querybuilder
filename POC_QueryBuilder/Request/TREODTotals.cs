﻿using CGP.Switch.Core.Shared.Types;
using Newtonsoft.Json;

namespace CGP.Switch.Core.Shared.Messages.EntryPoint.Requests
{
    /// <summary>
    /// TG338 TR  for endofday/Totals
    /// </summary>
    public class TREODTotals
    {
        #region Properties
        [JsonProperty(Required = Required.Always)]
        public Versioninfo VersionInfo { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Terminaltransid TerminalTransID { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Posdata POSData { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Transactiondata TransactionData { get; set; }

        public Sessiondata Sessiondata { get; set; }

        #endregion
    }
}
