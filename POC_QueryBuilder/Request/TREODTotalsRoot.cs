﻿
using Newtonsoft.Json;

namespace CGP.Switch.Core.Shared.Messages.EntryPoint.Requests
{
    /// <summary>
    /// TG338 TR Root for endofday/Totals
    /// </summary>
    public class TREODTotalsRoot
    {

        [JsonProperty("TR-REQ")]
        public TREODTotals TRREQ { get; set; }
    }
}
