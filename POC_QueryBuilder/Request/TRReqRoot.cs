﻿using Newtonsoft.Json;

namespace CGP.Switch.Core.Shared.EntryPoint.Requests
{
    public class TRReqRoot
    {
        [JsonProperty("TR-REQ")]
        public TRReq TRREQ { get; set; }
    }
}
