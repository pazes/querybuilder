﻿using CGP.Switch.Core.Shared.Types;
using Newtonsoft.Json;

namespace CGP.Switch.Core.Shared.EntryPoint.Requests
{
    public class TRCReq
    {
        [JsonProperty(Required = Required.Always)]
        public Versioninfo VersionInfo { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Entrypointtransid EntryPointTransID { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Entrypointtransid OriginalEntryPointTransID { get; set; }
        [JsonProperty(Required = Required.Always)]
        public TransactiondataCancelation TransactionData { get; set; }
    }
}
