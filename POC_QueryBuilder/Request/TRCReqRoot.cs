﻿using Newtonsoft.Json;

namespace CGP.Switch.Core.Shared.EntryPoint.Requests
{
    public class TRCReqRoot
    {
        [JsonProperty("TRC-REQ")]
        public TRCReq TRCREQ { get; set; }
    }
}
