﻿using CGP.Switch.Core.Shared.Types;
using Newtonsoft.Json;

namespace CGP.Switch.Core.Shared.EntryPoint.Requests
{
    public partial class TRReq
    {
        #region Properties
        [JsonProperty(Required = Required.Always)]
        public Versioninfo VersionInfo { get; set; }
        public Entrypointtransid EntryPointTransID { get; set; }
        public Terminaltransid TerminalTransID { get; set; }
        public Originalterminaltransid OriginalTerminalTransID { get; set; }
        public Posdata POSData { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Transactiondata TransactionData { get; set; }
        public Carddata CardData { get; set; }
        public IdentificationData identificationData { get; set; }
        public Amountrequestdata AmountRequestData { get; set; }
        public Securitydata SecurityData { get; set; }
        public string AdditionalTransReqData { get; set; }
        public Keyupdaterequestdata KeyUpdateRequestData { get; set; }
        [JsonProperty("3DSecureData")]
        public SecureData3D SecureData3D { get; set; }
        public Sessiondata Sessiondata { get; set; }
        public PaymentToken PaymentToken { get; set; }

        #endregion
    }
}