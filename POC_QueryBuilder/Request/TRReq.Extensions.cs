﻿using Newtonsoft.Json;
using System;

namespace CGP.Switch.Core.Shared.EntryPoint.Requests
{
    public partial class TRReq
    {
        #region Getters
        

        /// <summary>
        ///Get capture method string
        /// </summary>
        [JsonIgnore]
        public string GetCaptureMethod => TransactionData?.POSEntryMode?.CardDataCaptureMethod?.ToUpperInvariant();



        /// <summary>
        /// Return the intrack format
        /// </summary>
        [JsonIgnore]
        public string GetIntrackFormat
        {
            get
            {
                string captureMethod = GetCaptureMethod;
                string inTrackFormat = null;// Invalid value

                if (!string.IsNullOrEmpty(captureMethod))
                {
                    switch (captureMethod)
                    {
                        case "2": // Magstripe
                        case "N": // Contactless magstripe
                        case "S": // EMV CHIP Magstripe fallback
                        case "K": // Contactless magstripe Mobile 
                            inTrackFormat = "1";
                            break;

                        case "5": // EMV Chip
                        case "M": // EMV contactless
                        case "L": // Contactless Mobile
                            inTrackFormat = "1";
                            break;

                        case "6": // Manual entry
                        case "1": // Manual entry No POS
                        case "T": // EMV CHIP Manual entry fallback
                            inTrackFormat = "2";
                            break;

                        default:
                            break;
                    }
                }

                return inTrackFormat;
            }
        }

        public bool IsSaleOrCashout()
        {
            return (TransactionData?.TransactionClass == "02" && TransactionData?.TransactionID == "00");
        }
        public bool IsSale()
        {
            return (IsSaleOrCashout() && !IsCashout());
        }

        public bool IsSaleDCC()
        {
            bool resp = false;


            return resp;
        }

        public bool IsDCC()
        {
            return (TransactionData?.TransactionClass == "06" && TransactionData?.TransactionID == "10");
        }
        public bool IsCashout()
        {
            return (IsSaleOrCashout()
                && TransactionData?.CashOutTransaction != null
                && TransactionData?.CashOutTransaction == true);
        }


        public bool IsRealex()
        {
            bool isOK = TransactionData?.TerminalTransactionNumber.HasValue == false
                && !string.IsNullOrEmpty(identificationData?.OrderId);

            //no hace falta el responseCode           
            return isOK;
        }

        public bool IsTransactionStatus()
        {
            return (string.IsNullOrEmpty(CardData?.eCardData)
                && string.IsNullOrEmpty(CardData?.BIN)
                && TransactionData != null
                && !string.IsNullOrEmpty(TransactionData.TransactionClass)
                && !string.IsNullOrEmpty(TransactionData.TransactionID)
                && TransactionData.TransactionClass.Equals("02")
                && TransactionData.TransactionID.Equals("00")
                && OriginalTerminalTransID != null
                && !string.IsNullOrEmpty(OriginalTerminalTransID?.TimeStamp)
                && OriginalTerminalTransID?.TransNumber != null
                );
        }

        /// <summary>
        /// Verify if is chip capture method
        /// </summary>
        [JsonIgnore]
        public bool IsChipCaptureMethod
        {
            get
            {
                string method = TransactionData?.POSEntryMode?.CardDataCaptureMethod;
                return  false;
            }
        }

        /// <summary>
        /// verify is chip or magstripe
        /// </summary>
        [JsonIgnore]
        public bool IsChipOrMagstripeCaptureMethod
        {
            get
            {
                string method = TransactionData?.POSEntryMode?.CardDataCaptureMethod;
                return method != null;
            }
        }

        [JsonIgnore]
        public bool IsRetryOperation
        {
            get
            {
                bool isPOSRepeatIndicator = TerminalTransID?.RepeatIndicator ?? false;
                bool isEntryPointRepeatIndicator = EntryPointTransID?.RepeatIndicator ?? false;

                return isPOSRepeatIndicator || isEntryPointRepeatIndicator;
            }
        }



        public bool IsPreAuthorizationSubstitution()
        {
            return (TransactionData?.OrigTerminalTransactionNumber ?? -1) > 0;
        }

        public bool IsEndOfDay()
        {
            return (TransactionData?.TransactionClass == "05" && TransactionData?.TransactionID == "00");
        }


        #endregion
    }
}