﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CGP.Switch.Core.Shared.Types
{
    #region OperationContext

    public class OperationContext
    {
        public Track2Data Track2Data { get; set; }
        public IsDCC IsDCC { get; set; }
        public string ErrorUpdatingTotals { get; set; }
    }

    #endregion

    #region IsDCC
    public class IsDCC
    {
        public string DCCType { get; set; }
        public string MarkUp { get; set; }
        public string CardLang { get; set; }
        public string CardCountry { get; set; }
        public int? Exchange { get; set; }
        public int? ExchangeBit60 { get; set; }
        public string CurrencyExponentForeignAmount { get; set; }
        public string CurrencyExponentLocalAmount { get; set; }
        public string CardCurrency3 { get; set; }


    }
    #endregion

    #region Originalterminaltransid

    /// <summary>
    /// Data used to identify a previous transaction done by the terminal.
    /// Data taken from the terminal request to identify a previous transaction
    /// Data to identify a previous transaction as originated in the POS, used in transaction class 04 - Cancellations
    /// </summary>
    /// <remarks>Data to identify a previous transaction as originated in the POS, used in transaction class 04 - Cancellations</remarks>
    public class Originalterminaltransid
    {
        public string TimeStamp { get; set; }
        public int TransNumber { get; set; }
    }

    #endregion

    #region Paramsversion

    public class Paramsversion
    {
        /// <summary>
        /// Terminal parameters version
        /// </summary>
        /// <remarks>Version of the parameters present in the POS related to financial transactions configuration. Included in v1.12</remarks>
        public int? FinancParamsVersion { get; set; }
        /// <summary>
        /// EMV RSA public keys version
        /// </summary>
        /// <remarks>Version identifying the set of RSA CA keys present in the terminal. Included in v1.12</remarks>
        public int? EMVKeysVersion { get; set; }
    }

    #endregion

    #region Pinsecdata
    public class Pinsecdata
    {
        /// <summary>
        /// Key serial number
        /// </summary>
        /// <value>String – HEX 20</value>
        public string KSN { get; set; }


        /// <summary>
        /// Determines the length of the PIN entered for online verification
        /// </summary>
        /// <value>Numeric – N2</value>
        /// <remarks>No present if no PIN is included in the request</remarks>
        public int? PINLength { get; set; }

        /// <summary>
        /// Ciphered block containing the PIN.
        /// </summary>
        /// <remarks>No present if no PIN is included in the request</remarks>
        /// <value>String – HEX 16</value>
        public string PINBlock { get; set; }

        /// <summary>
        /// Key index
        /// </summary>
        /// <remarks>No present if no PIN is included in the request</remarks>
        /// <value>Numeric – 2N</value>
        public int? KeyIndex { get; set; }

        /// <summary>
        /// Random number (included in v1.13) - String – HEX 6 
        /// </summary>
        /// <remarks>Only present in RE-REQ, Index of the key used to cipher card data in H2H</remarks>
        public string RndNumber { get; set; }
    }

    #endregion

    #region Posentrymode

    public class Posentrymode
    {
        public string CardDataCaptureMethod { get; set; }
        public int? CardholderAuthMethod { get; set; }
    }

    #endregion

    #region TicketDefinitions


    /// <summary>
    /// Static labels applied to TicketData.
    /// Remeber that _XXXX_ labels are only for working purposes and will be deleted at the end of the ticket generation
    /// </summary>
    public static class TicketDefinitions
    {
        public const int DefaultLogoIdentifier = 0;

        public const string Logo = "Logo";

        public const string AID = "AID";
        public const string ATC = "ATC";
        public const string ApplicationLabel = "ApplicationLabel";

        public const string ARC = "ARC";

        public const string EMVICCData = "_EMVICCData_";
        public const string AuthMethod = "AuthMethod";
        public const string AuthMethodText = "AuthMethodText";

        public static class Operation
        {
            public const string Id = "OperationId";
            public const string Class = "OperationClass";
            public const string Type = "OperationTypeString";
            public const string CurrencyCode = "OperationCurrencyCode";
            public const string Currency = "OperationCurrency";
            public const string Amount = "OperationAmount";
            public const string Result = "OperationResult";

            public const string Timestamp = "OperationTimestamp";
            public const string Number = "OperationNumber";
            public const string Comision = "Comision";

        }
        public static class Merchant
        {
            public const string Id = "MerchantId";
            public const string Name = "MerchantName";
            public const string Address = "MerchantAddress";
        }

        public static class CardData
        {
            public const string CaptureMethodId = "CardDataCaptureMethodId";
            public const string CaptureMethodText = "CardDataCaptureMethod"; // TODO: No esta el literal en los templates
            public const string CardNumber = "CardNumber";
            public const string CardSequenceNumber = "CardSequenceNumber";

            public const string CardholderAuthMethod = "CardholderAuthMethod";
        }

        public static class SwitchOperationData
        {
            public const string OperationId = "OperationId";
            public const string Date = "OperationDate";
            public const string Time = "OperationTime";

        };

        public static class Totals
        {
            public const string SalesNumber = "TotalsSalesNumber";
            public const string SalesAmount = "TotalsSalesAmount";

            public const string RefundsNumber = "TotalsRefundsNumber";
            public const string RefundsAmount = "TotalsRefundsAmount";

            public const string OperationsNumber = "TotalsOperationsNumber";
            public const string OperationsAmount = "TotalsOperationsAmount";
        }

        public static class Processor
        {
            public const string DefaultProcessorName = "Redsys";

            public const string Name = "ProcessorName";
            public const string Response = "ProcessorResponse";
            //public const string ResponseCode = "ProcessorResponseCode";
            public const string Timestamp = "ProcessorTimestamp";
            public const string AuthorizationNumber = "ProcessorAuthorizationNumber";
            public const string TransactionNumber = "ProcessorTransNumber";
            public const string TerminalId = "ProcessorTerminalId";
            public const string MerchantId = "ProcessorMerchantId";
        }

        public static class Terminal
        {
            public const string Id = "TerminalId";
            public const string Language = "TerminalLanguage";
            public const string Timezone = "TerminalTimeZone";
            public const string Location = "TerminalLocation";
            public const string TransactionNumber = "TerminalTransactionNumber";
        }


        public static class TicketStrings
        {
            public const string TimeString = "OperationTimeString";
            public const string DateString = "OperationDateString";
            public const string AmountString = "OperationAmountString";
        }

        public static class ManualClose
        {
            public const string Operation = "ManualCloseOperation";
            public const string Session = "ManualCloseSession";
            public const string RangeFrom = "ManualCloseRangeFrom";
            public const string RangeTo = "ManualCloseRangeTo";
        }
        public static class Operations
        {
            public const string OperationsRange = "OperationsRange";
            public const string DatesRange = "OperationsDatesRange";
            public const string Session = "OperationsSession";

            public const string SalesNumber = "OperationsSalesNumber";
            public const string SalesAmount = "OperationsSalesAmount";

            public const string RefundsNumber = "OperationsRefundsNumber";
            public const string RefundsAmount = "OperationsRefundsAmount";

            public const string OperationsNumber = "OperationsNumber";
            public const string OperationsAmount = "OperationsAmount";
        }
        public static class MultiCurrencyOffered
        {
            public const string SalesNumber = "MultiCurrencyOfferedSalesNumber";
            public const string SalesAmount = "MultiCurrencyOfferedSalesAmount";

            public const string RefundsNumber = "MultiCurrencyOfferedRefundsNumber";
            public const string RefundsAmount = "MultiCurrencyOfferedRefundsAmount";

            public const string OperationsNumber = "MultiCurrencyOfferedOperationsNumber";
            public const string OperationsAmount = "MultiCurrencyOfferedOperationsAmount";
        }
        public static class MultiCurrencyAccepted
        {
            public const string SalesNumber = "MultiCurrencyAcceptedSalesNumber";
            public const string SalesAmount = "MultiCurrencyAcceptedSalesAmount";

            public const string RefundsNumber = "MultiCurrencyAcceptedRefundsNumber";
            public const string RefundsAmount = "MultiCurrencyAcceptedRefundsAmount";

            public const string OperationsNumber = "MultiCurrencyAcceptedOperationsNumber";
            public const string OperationsAmount = "MultiCurrencyAcceptedOperationsAmount";
        }

    }

    #endregion

    #region Track2Data

    public class Track2Data
    {
        // Always stored Encrypted
        public string PAN { get; set; }

        public string ExpirationDate { get; set; }

        public string ServiceCode { get; set; }

        public string DiscretionaryData { get; set; }

        public int ExpirationYear => int.Parse(ExpirationDate.Substring(0, 2));

        public int ExpirationMonth => int.Parse(ExpirationDate.Substring(2, 2));

        /// <summary>
        /// Four last chars of the PAN in clear.
        /// </summary>
        public string ClearEndPan { get; set; }

        public Track2Data()
        {
        }


    }

    #endregion

    #region Transactiondata

    public class TransactiondataCancelation : Transactiondata
    {
        [JsonProperty(Required = Required.Default)]
        public new string TransactionClass { get; set; }

        [JsonProperty(Required = Required.Default)]
        public new string TransactionID { get; set; }

        public int? CoreDuplicated { get; set; }
    }

    public class TransactiondataRE
        : Transactiondata
    {
        public string orderId { get; set; }
    }

    public class Transactiondata
    {
        [JsonProperty(Required = Required.Always)]
        public string TransactionClass { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string TransactionID { get; set; }

        //JC: EndOfDay not required: [JsonProperty(Required = Required.Always)]
        public Posentrymode POSEntryMode { get; set; }
        public int? TerminalTransactionNumber { get; set; }

        public bool RecurringTransaction { get; set; } = false;

        // SOLO DCC
        //private bool _DCCEnabled = false;
        public bool? DCCEnabled;

        public string DCCIndicator;

        public string AuthorizationNumber;

        /// <summary>
        /// Terminal transaction identification - N6
        /// </summary>
        /// <remarks>Field used to make a reference to a previous transaction used in the interface between the entry point and the core.</remarks>
        public int? OrigTerminalTransactionNumber { get; set; }

        /// <summary>
        /// Reason of the communication. Specifically used in cancellations
        ///     00 – Timeout expired
        ///     01 – Unable to complete the transaction locally
        ///     02 – Transaction declined by the card in the 2nd GAC
        /// </summary>
        public int? ReasonCode { get; set; }

        /// <summary>
        ///  CashOutEnabled - Boolean
        /// </summary>
        /// <remarks>Flag that determines whether the terminal can process a cashout transaction after purchase of goods or services with same card.
        /// In TR-REQ this field informs that POS has the capability and that the transaction is suitable for a possible cashout transaction coming next. 
        ///In RE-REQ informs the processor about the possibility of carrying out a cashout transaction immediately after.
        /// </remarks>
        public bool? CashOutEnabled { get; set; }

        /// <summary>
        ///  CashOutTransaction - Boolean
        /// </summary>
        /// <remarks>Flag that determines that the current transaction is cashout. </remarks>
        public bool? CashOutTransaction { get; set; }

        /// <summary>
        ///  PSD2Transaction - Boolean
        /// </summary>
        /// <remarks>Flag that determines that the current transaction is sent after an SCA mechanism was applied in the POI side. 
        /// In other words, single tap with PIN was performed after the original transaction was rejected. It also means that the 
        /// EMV data is the same as the previous transaction that was rejected. 
        /// </remarks>
        public bool? PSD2Transaction { get; set; }

        public int? CoreDuplicated { get; set; }

        public Transactiondata()
        {
            OrigTerminalTransactionNumber = 0;
        }



        [JsonIgnore]
        public bool IsContactlessMethod
        {
            get
            {
                string captureMethod = POSEntryMode?.CardDataCaptureMethod.ToUpperInvariant();
                bool isContactLess = false;

                if (!string.IsNullOrEmpty(captureMethod))
                {
                    switch (captureMethod)
                    {
                        //case "5": // EMV Chip
                        //case "T": // EMV CHIP Manual entry fallback
                        case "M": // EMV contactless
                        case "L": // Contactless Mobile
                        case "K": // Contactless magstripe Mobile
                        case "N": // Contactless magstripe
                            isContactLess = true;
                            break;

                        default:
                            isContactLess = false;
                            break;
                    }
                }
                return isContactLess;
            }
        }
    }

    #endregion

    #region Versioninfo

    public class Versioninfo
    {
        public string Version { get; set; }
        public string Release { get; set; }
    }

    #endregion

    #region UnbalanceFile

    public class UnbalanceFile
    {
        public UnbalanceFile()
        {
            Sends = new List<Send>();
        }
        public string id { get; set; }
        public string SessionId { get; set; }
        public string MerchantID { get; set; }
        public string TerminalID { get; set; }
        public DateTime CreateDate { get; set; }
        public string PathToFile { get; set; }
        public StatusType Status { get; set; }
        public int NumSends { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public DateTime? LastErrorDate { get; set; }
        public string CosmoLink { get; set; }
        public List<Send> Sends { get; set; }
    }

    public class Send
    {
        public DateTime CreateDate { get; set; }
        public StatusType SendStatus { get; set; }
        public string TextError { get; set; }
    }

    public enum StatusType
    {
        ToSend,
        Sent,
        Error
    }

    #endregion
}
