﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace CGP.Switch.Core.Shared.Types
{
    //Type container for all Response components

    #region Amountrespdata

    public class Amountrespdata
    {
        public int? ApprovedAmount { get; set; }
        public int? ConciliationAmount { get; set; }
        public int? ForeignCurrencyAmount { get; set; }

        public int? TransactionConvRate { get; set; }
        public int? ConciliationConvRate { get; set; }

        public int? LocalCurrencyAmount { get; set; }

        public int? LocalCurrencyCode { get; set; }
        public int? ForeignCurrencyCode { get; set; }
        public int? ConciliationCurrencyCode { get; set; }

        public string MarkUpMsg { get; set; }
        public string CommissionMsg { get; set; }
        public string DCCProviderMsg { get; set; }
        public string DCCDisclaimerMsg { get; set; }

        public string ForeignCurrencyName { get; set; }
        public string ForeignCurrencyAbbreviation { get; set; }
        public int? ForeignCurrencyFormat { get; set; }





    }


    #endregion

    #region DateFormatConverter

    public class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }

    #endregion

    #region Coretransid

    public class Coretransid
    {

        #region Constants
        public const string TimestampPattern = "yyyyMMddHHmmss";

        #endregion

        #region Properties

        public string CoreID { get; set; }

        [JsonIgnore]
        public const string TIMESTAMP_FORMAT = "yyMMddHHmmss";

        [JsonConverter(typeof(DateFormatConverter), TIMESTAMP_FORMAT)]
        public DateTime TimeStamp { get; set; }
        public int? TransNumber { get; set; }

        [JsonIgnore]
        public string GetSwitchOperationId => !IsValid
                    ? null
                    : $"{TimeStamp.ToString(TimestampPattern)}.{TransNumber:D5}";

        [JsonIgnore]
        public DateTime? GetSwitchOperationTimestamp
        {
            get
            {
                DateTime? res = null;

                if (IsValid) { res = TimeStamp; }

                return res;
            }
        }

        [JsonIgnore]
        public bool IsValid => !string.IsNullOrEmpty(CoreID) &&
                TimeStamp != DateTime.MinValue &&
                TransNumber.HasValue;


        public string CreateUniqueIdentifierForTransaction(uint deviceId)
        {
            return //coreTransID == null || 
                !IsValid
                ? null
                : $"{deviceId:D10}.{CoreID.Replace(' ', '_')}.{TimeStamp.ToString(TimestampPattern)}.{TransNumber:D5}";
        }

        #endregion
    }

    #endregion

    #region CoreTypeFactory

    public class CoreTypeFactory
    {
        public static Versioninfo VersionInfo => new Versioninfo() { Version = "1.0.0.0", Release = "0.0" };

        public static Versioninfo VersionInfoV2 => new Versioninfo() { Version = "02.00", Release = "02.00" };

        public static Versioninfo BuilVersion(int version)
        {
            return version >= 2 ? VersionInfoV2 : VersionInfo;
        }
    }

    #endregion

    #region Exitpointtransid

    public class Exitpointtransid
    {
        public Exitpointtransid()
        {
            ExitPointID = "";
            TimeStamp = DateTime.Now;
            TransNumber = 0;
        }

        [JsonRequired]
        public string ExitPointID { get; set; }
        [JsonRequired]
        public DateTime TimeStamp { get; set; }
        [JsonRequired]
        public int TransNumber { get; set; }
    }


    #endregion

    #region GenSecurityRespData

    public class GenSecurityRespData
    {
    }

    #endregion

    #region Gsmsimid

    public class Gsmsimid
    {
        public string ICCID { get; set; }
    }

    #endregion

    #region Processingresult

    public class Processingresult
    {
        public Processingresult()
        {
            ProcessingStatus = 0;
        }

        [JsonRequired]
        public int? ProcessingStatus { get; set; }
        public string ExtendedInfo { get; set; }
    }

    #endregion

    #region Processordata
    public class Processordata
    {
        public string MerchantID { get; set; }
        public string TerminalID { get; set; }
        public Sessiondata SessionData { get; set; }
        public uint Acquirer { get; set; }
    }

    #endregion

    #region Sessiondata

    public class Sessiondata
    {
        public string Date { get; set; }
        public int? Number { get; set; }
    }

    #endregion

    #region Terminalindicators

    public class Terminalindicators
    {
        public Upload Upload { get; set; }
        public Download Download { get; set; }
        public Keyrenovation KeyRenovation { get; set; }
        public Keyremoval KeyRemoval { get; set; }
    }

    #endregion

    #region Upload

    public class Upload
    {
        public bool OfflineTrans { get; set; }
        public bool Signatures { get; set; }
    }

    #endregion

    #region Download

    public class Download
    {
    }

    #endregion

    #region Keyrenovation

    public class Keyrenovation
    {
        public string HostRandomNumber { get; set; }
    }

    #endregion

    #region Keyremoval

    public class Keyremoval
    {
        public string HostRandomNumber { get; set; }
    }

    #endregion

    #region Terminal


    /// <summary>
    /// Data in trasaction result for Terminal
    /// </summary>
    public class Terminal
    {
        public string Display { get; set; }
        public Printer Printer { get; set; }
        public string TemplateRespData { get; set; }
    }

    #endregion

    #region TemplateRespData
    public class TemplateRespData
    {
        public TemplateRespData()
        {
            Templates = new List<Templates>();
            Modifiers = new List<Modifiers>();
        }

        [JsonProperty(Required = Required.Always)]
        public string Type { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Status { get; set; }

        public string Id { get; set; }
        public string Authorization { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Amount { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Currency { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Location { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string CardBank { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string CardNumber { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string CardType { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string CardHolder { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Date { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Time { get; set; }

        public string OriginalTransactionId { get; set; }
        public string OriginalTransactionDate { get; set; }
        public List<Templates> Templates { get; set; }
        //[JsonProperty(Required = Required.Always)]
        public string CardIssuer { get; set; }
        public string ATC { get; set; }
        public string AID { get; set; }
        public string ARC { get; set; }
        public string PSN { get; set; }
        public string TerminalId { get; set; }
        public string MerchantId { get; set; }
        public string MerchantName { get; set; }
        public int SignatureIndicator { get; set; }
        public int PinIndicator { get; set; }
        public string Language { get; set; }
        public List<Modifiers> Modifiers { get; set; }

        public CashOutInfoData CashOutInfoData { get; set; }
    }
    public class Templates
    {

        public string Id { get; set; }
        public string Target { get; set; }
        public string Data { get; set; }

    }
    public class Modifiers
    {
        public string Id { get; set; }
        public int Status { get; set; }
        public string Currency { get; set; }
        public string Amount { get; set; }
        public string AmountnoMarkup { get; set; }
        public string DCCType { get; set; }
        public string Markup { get; set; }
        public string CARDlang { get; set; }
        public string CARDcountry { get; set; }
        public string Exchange { get; set; }
        public string CurrencyExponent { get; set; }

    }

    #endregion

    #region Printer

    /// <summary>
    /// Printer specific data (ticket)
    /// </summary>
    public class Printer
    {
        /// <summary>
        /// Merchant ticket
        /// </summary>
        public string MerchantTicket { get; set; }
        /// <summary>
        /// Customer ticket
        /// </summary>
        public string CustomerTicket { get; set; }
    }

    #endregion

    #region Totalsdata

    public class Totalsdata
    {
        /// <summary>
        /// Número de devoluciones 
        /// </summary>
        /// <remarks>Devolución</remarks>
        public int RefundsNumber { get; set; }

        /// <summary>
        /// Número de cancelaciones de autorización 
        /// </summary>
        /// <remarks>Cancelaciones de venta, confirmación de preautorización</remarks>
        public int AuthCancellationsNumber { get; set; }

        /// <summary>
        /// Número de autorizaciones 
        /// </summary>
        /// <remarks>Venta (ON/OFF), confirmación de preautorización</remarks>
        public int AuthorizationsNumber { get; set; }

        /// <summary>
        /// Importe de cancelaciones de devolución 
        /// </summary>
        /// <remarks>Cancelación de devolución </remarks>
        public int RefundCancellationsNumber { get; set; }

        /// <summary>
        /// Importe de devoluciones 
        /// </summary>
        /// <remarks>Devolución </remarks>
        public int RefundsAmount { get; set; }

        /// <summary>
        /// Importe de cancelaciones de autorización 
        /// </summary>
        /// <remarks>Cancelaciones de venta, confirmación de preautorización </remarks>
        public int AuthCancellationsAmount { get; set; }

        /// <summary>
        /// Importe de autorizaciones 
        /// </summary>
        /// <remarks>Venta (ON/OFF), confirmación de preautorización</remarks>
        public int AuthorizationsAmount { get; set; }

        /// <summary>
        /// Número de cancelaciones de devolución 
        /// </summary>
        /// <remarks>Cancelación de devolución</remarks>
        public int RefundCancellationsAmount { get; set; }


        public int TotalAmount { get; set; }

        //[JsonIgnoreAttribute]
        public DateTime RangeTimestampFrom { get; set; }
        //[JsonIgnoreAttribute]
        public DateTime RangeTimestampTo { get; set; }
        //[JsonIgnoreAttribute]
        public string RangeOperationFrom { get; set; }
        //[JsonIgnoreAttribute]
        public string RangeOperationTo { get; set; }
    }

    #endregion

    #region Totalsdata

    public class TotalsInfodata
    {

        public int RatioConverted { get; set; }
    }

    #endregion

    #region Transactionresult

    public class Transactionresult
    {
        public int? ResponseCode { get; set; }

        private string _authorizationNumber;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AuthorizationNumber
        {
            get => _authorizationNumber;
            set => _authorizationNumber = string.IsNullOrEmpty(value) ? null : value.PadLeft(6, ' ');
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EMVICCIssuerData { get; set; }

        public Terminal Terminal { get; set; }

        public int? TerminalTransactionNumber { get; set; }

        // TransactionStatus
        public int? TransactionStatus { get; set; }

        public int? OrigTerminalTransactionNumber { get; set; }

        public bool? CashOutEnabled { get; set; }


    }

    #endregion

    #region EndOfdayTypes
    //*************************************************************************
    //* TG338 TR Resp   for endofday/Totals
    //*************************************************************************
    public class EODTotalsData
    {
        public EODTotalsData()
        {
            OpsData = new OpsData();
        }

        public string FirstOp { get; set; }


        public string LastOp { get; set; }


        public string FirstDate { get; set; }


        public string LastDate { get; set; }


        public string Date { get; set; }

        public string Symbol { get; set; }

        public OpsData OpsData { get; set; }
        public MultiCurrencyData MultiCurrencyData { get; set; }

        public SplitPaymentData SplitPaymentData { get; set; }

        public TaxFreeData TaxFreeData { get; set; }

    }
    /* old (20181008)
    public class Ticket
    {

        public Ticket()
        {
            Promo Promo = new Promo();
        }
        [JsonProperty(Required = Required.Always)]
        public string TicketHeader { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Place { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Store { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Tpv { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Session { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Date { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Time { get; set; }

        public Promo Promo { get; set; }


    }
    */
    public class Promo
    {
        public string PromoTitle { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string PromoText { get; set; }

    }

    public class OpsData
    {
        public OpsData()
        {
            Rows = new List<Row>();
        }
        [JsonProperty(Required = Required.Always)]
        public List<Row> Rows { get; set; }


    }

    public class Row
    {
        [JsonProperty(Required = Required.Always)]
        public string RowTitle { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string TicketTitle { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int Amount { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int Value { get; set; }
    }
    /*    public class Op
            : Operation
        {

            [JsonProperty(Required = Required.Always)]
            public string OpAmount { get; set; }
            [JsonProperty(Required = Required.Always)]
            public string Card { get; set; }
            [JsonProperty(Required = Required.Always)]
            public string Date { get; set; }
            [JsonProperty(Required = Required.Always)]
            public string Time { get; set; }
            [JsonProperty(Required = Required.Always)]
            public string OpId { get; set; }
            public string OriginalOp { get; set; }
            [JsonProperty(Required = Required.Always)]
            public string OpType { get; set; }
            [JsonProperty(Required = Required.Always)]
            public string TicketType { get; set; }

        }
    */
    public class MultiCurrencyData
    {
        [JsonProperty(Required = Required.Always)]
        public List<Row> Rows { get; set; }
        public List<SusceptibleMulticurrency> SusceptibleMulticurrency { get; set; }
        public List<ConvertedMulticurrency> ConvertedMulticurrency { get; set; }

    }
    public class SusceptibleMulticurrency
    {
        [JsonProperty(Required = Required.Always)]
        public string Currency { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Amount { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Value { get; set; }
    }

    public class ConvertedMulticurrency
    {
        [JsonProperty(Required = Required.Always)]
        public string Currency { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Amount { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Value { get; set; }
    }

    public class SplitPaymentData
    {
        [JsonProperty(Required = Required.Always)]
        public List<Row> Rows { get; set; }
        public List<SusceptibleSplitpayment> SusceptibleSplitpayment { get; set; }
        public List<ConvertedSplitpayment> ConvertedSplitpayment { get; set; }
    }

    public class SusceptibleSplitpayment
    {

        [JsonProperty(Required = Required.Always)]
        public decimal Amount { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Value { get; set; }
    }

    public class ConvertedSplitpayment
    {
        [JsonProperty(Required = Required.Always)]
        public int Months { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Amount { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Value { get; set; }

    }

    public class TaxFreeData
    {
        [JsonProperty(Required = Required.Always)]
        public List<Row> Rows { get; set; }
        public List<SusceptibleTaxfree> SusceptibleTaxfree { get; set; }
        public List<ConvertedTaxfree> ConvertedTaxfree { get; set; }
    }
    public class SusceptibleTaxfree
    {
        [JsonProperty(Required = Required.Always)]
        public string Currency { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Amount { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Value { get; set; }

    }
    public class ConvertedTaxfree
    {
        [JsonProperty(Required = Required.Always)]
        public string Currency { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Amount { get; set; }

        [JsonProperty(Required = Required.Always)]
        public decimal Value { get; set; }

    }
    #endregion

    #region Receipt Data

    /// <summary>
    /// Receipt data with information for html templates
    /// </summary>
    public class ReceiptData
    {
        [JsonProperty(Required = Required.Always)]
        public int TemplateId { get; set; }
        public bool Contactless { get; set; }
        public string HeaderPromoTitle { get; set; }
        public string HeaderPromoText { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string LocationDate { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string CommerceName { get; set; }
        public bool Return { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string OperationAmount { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string DataLeft { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string DataRight { get; set; }
        public string Footer { get; set; }
        public string Signature { get; set; }
        public string PromoTitle1 { get; set; }
        public string PromoText1 { get; set; }
        public string PromoTitle2 { get; set; }
        public string PromoText2 { get; set; }
    }

    #endregion

    #region CashOutInfoData
    public class CashOutInfoData
    {
        public int? AcquirerCommissionType { get; set; }
        public string AcquirerCommissionAmount { get; set; }
        public string CommissionCurrency { get; set; }
        public int? AdditionalCommissionType { get; set; }
        public string AdditionalCommissionAmount { get; set; }
    }
    #endregion
}
