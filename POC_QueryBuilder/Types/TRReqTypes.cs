﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CGP.Switch.Core.Shared.Types
{
    //Type container for all Request components

    #region AdditionalTransReqData
    /// <summary>
    /// AdditionalTransReqData
    /// </summary>
    public class AdditionalTransReqData
    {
    }


    #endregion

    #region Amountrequestdata

    public class Amountrequestdata
    {
        #region Properties
        public int? TransactionAmount { get; set; }
        public int? TransactionCurrency { get; set; }
        public int? OrigTransactionAmount { get; set; }

        public int? LocalCurrencyAmount { get; set; }
        public int? LocalCurrencyCode { get; set; }
        public int? TransactionConvRate { get; set; }
        #endregion

        #region Constructor

        //public Amountrequestdata() => OrigTransactionAmount = 0;

        #endregion

        #region Public methods

        /// <summary>
        /// Validate the amount
        /// </summary>
        /// <param name="throwExceptionIfNotValid"></param>
        /// <returns></returns>
        public bool Validate(bool throwExceptionIfNotValid = true)
        {
            bool bOk = true;

            if (bOk)
            {
                bOk = TransactionAmount.HasValue;
                if (!bOk)
                {
                    if (throwExceptionIfNotValid)
                    {
                    }
                }
            }

            return bOk;
        }

        #endregion
    }

    #endregion

    #region CardBrand

    public sealed class CardBrand
    {
        public static readonly CardBrand Visa = new CardBrand("00", "Visa");
        public static readonly CardBrand MasterCard = new CardBrand("01", "MasterCard");
        public static readonly CardBrand Maestro = new CardBrand("01", "Maestro");
        public static readonly CardBrand AmericanExpress = new CardBrand("03", "AmericanExpress");
        public static readonly CardBrand DinnersClubUsaCanada = new CardBrand("05", "DinnersClubUsaCanada");
        public static readonly CardBrand DinnersClubEnroute = new CardBrand("05", "DinnersClubEnroute");
        public static readonly CardBrand DinnersClubInternational = new CardBrand("05", "DinnersClubInternational");


        public static readonly CardBrand OtrasMarcas = new CardBrand("99", "OtrasMarcas");
        private CardBrand(string value, string name)
        {
            StringValue = value;
            StringName = name;
        }

        public string StringValue { get; private set; }
        public string StringName { get; private set; }
    }

    #endregion

    #region Carddata
    public class Carddata
    {
        //JC: NO requido para cancelaciones[JsonRequired]
        public string BIN { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? SequenceNumber { get; set; }
        public string Track2Data { get; set; }
        // Stores eCommerce Card Data with format: MMyy(4)CVV(3)PAN(10..13)
        public string eCardData { get; set; }

        public string PayerReference { get; set; }
        public string CardReference { get; set; }

        [JsonIgnore]
        public string Track2DataClear { get; set; }

        // Hex
        public string KeyedCardData { get; set; }
        public string EMVICCData { get; set; }

        public Carddata()
        {
            //SequenceNumber = 0;
            BIN = null;
        }

        #region Public Methods
        [JsonIgnore]
        public CardBrand GetCardBrand
        {
            get
            {
                if (BIN == null) { return CardBrand.OtrasMarcas; }

                string s = BIN;

                int first1Digit = int.Parse(s.Substring(0, 1));
                int first2Digit = int.Parse(s.Substring(0, 2));
                int first3Digit = int.Parse(s.Substring(0, 3));
                int first4Digit = int.Parse(s.Substring(0, 4));


                if (first1Digit == 4)
                {
                    return CardBrand.Visa;
                }

                if ((first4Digit >= 2221 && first4Digit <= 2720) || (first2Digit >= 51 && first2Digit <= 55))
                {
                    return CardBrand.MasterCard;
                }

                if ((first2Digit == 50) || (first2Digit >= 56 && first2Digit <= 58) || (first1Digit == 6))
                {
                    return CardBrand.Maestro;
                }

                if (first2Digit >= 34 && first2Digit <= 37)
                {
                    return CardBrand.AmericanExpress;
                }

                if (first2Digit >= 54 && first2Digit <= 55)
                {
                    return CardBrand.DinnersClubUsaCanada;
                }

                if (first4Digit >= 2014 && first4Digit <= 2149)
                {
                    return CardBrand.DinnersClubEnroute;
                }

                if (first2Digit == 36 || (first3Digit >= 300 && first3Digit <= 305) || (first4Digit == 3095) || (first2Digit >= 38 && first2Digit <= 39))
                {
                    return CardBrand.DinnersClubInternational;
                }

                return CardBrand.OtrasMarcas;
            }
        }

        [JsonIgnore]
        public bool HasTrack2Data => !string.IsNullOrEmpty(Track2Data);
        [JsonIgnore]
        public bool HasKeyedCardData => !string.IsNullOrEmpty(KeyedCardData);

        #endregion
    }

    #endregion

    #region Cardsecdata

    public class Cardsecdata
    {
        /// <summary>
        /// Key serial number
        /// </summary>
        /// <value>String – HEX 20</value>
        public string KSN { get; set; }

        /// <summary>
        /// Key index
        /// </summary>
        /// <remarks>Only present in RE-REQ, Index of the key used to cipher card data in H2H</remarks>
        /// <value>Numeric – 2N</value>
        public int? KeyIndex { get; set; }

        /// <summary>
        /// Random number (included in v1.13) - String – HEX 6 
        /// </summary>
        /// <remarks>Only present in RE-REQ, Index of the key used to cipher card data in H2H</remarks>
        public string RndNumber { get; set; }
    }
    public class ECardSecData
    {
        /// <summary>
        /// Key index
        /// </summary>
        /// <remarks>Only present in RE-REQ, Index of the key used to cipher card data in H2H</remarks>
        /// <value>Numeric – 2N</value>
        public int? KeyIndex { get; set; }

        /// <summary>
        /// Random number (included in v1.13) - String – HEX 6 
        /// </summary>
        /// <remarks>Only present in RE-REQ, Index of the key used to cipher card data in H2H</remarks>
        public string RndNumber { get; set; }
    }

    #endregion

    #region Entrypointtransid
    public class Entrypointtransid
    {
        public string EntryPointID { get; set; }
        [JsonIgnore]
        public const string TIMESTAMP_FORMAT = "yyMMddHHmmss";
        public DateTime? TimeStamp { get; set; }
        public int? TransNumber { get; set; }
        public bool RepeatIndicator { get; set; }
    }

    #endregion

    #region Functionalities

    public class Functionalities
    {
        public int? DCCCapabilities { get; set; }
        public bool? DigitalSignatureCapture { get; set; }
        public bool? EMVOff { get; set; }
        public bool? MagStripeOff { get; set; }
        public bool? TemplateRespData { get; set; }
        public bool? PSD2Capability { get; set; }
    }


    #endregion

    #region Keysversion
    public class Keysversion
    {
        public string KEKKAUTVersion { get; set; }
        public string KEKVersion { get; set; }
        public string KEKKCV { get; set; }
        public string KAUTVersion { get; set; }
        public string KAUTKCV { get; set; }
        public string KPINVersion { get; set; }
        public string KPINKCV { get; set; }
        public string KPANVersion { get; set; }
        public string KPANKCV { get; set; }
        public string KMACVersion { get; set; }
        public string KMACKCV { get; set; }
    }

    #endregion

    #region Keyupdaterequestdata

    public class Keyupdaterequestdata
    {
        public string TerminalSignature { get; set; }
        public string TerminalRandomNumber { get; set; }
    }


    #endregion

    #region PosdataExit

    public class PosdataExit
    {
        public Functionalities Functionalities { get; set; }
    }

    #endregion

    #region Posdata

    public class Posdata
    {
        [JsonProperty(Required = Required.Always)]
        public Terminalid TerminalID { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Softwareid SoftwareID { get; set; }

        public Gsmsimid GSMSIMId { get; set; }

        // JC: No requerido en EOF
        // [JsonProperty(Required = Required.Always)]
        public Keysversion KeysVersion { get; set; }

        public Paramsversion ParamsVersion { get; set; }
        public Functionalities Functionalities { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Terminalconfiguration TerminalConfiguration { get; set; }
    }

    #endregion

    #region  Securitydata
    public class Securitydata
    {
        public Pinsecdata PINSecData { get; set; }
        public Cardsecdata CardSecData { get; set; }
        [JsonProperty("eCardSecData")]
        public ECardSecData ECardSecData { get; set; }
    }

    #endregion

    #region Softwareid

    public class Softwareid
    {
        public string AppName { get; set; }
        public string AppVersion { get; set; }
        public string AppRelease { get; set; }
        public string AppDate { get; set; }
    }

    #endregion

    #region Terminalconfiguration

    public class Terminalconfiguration
    {
        public string TerminalLanguage { get; set; }
        public string TerminalTimeZone { get; set; }
        public int? SignatureIndicator { get; set; }
        public int? OffLineTransIndicator { get; set; }
        /// <summary>
        /// Number of failed EMV transactions not yet uploaded
        /// </summary>
        public int? EMVFailedIndicator { get; set; }
    }

    #endregion

    #region Terminalid
    public class Terminalid
    {
        public string ManufacturerCode { get; set; }
        public string FamilyCode { get; set; }
        public string ModelCode { get; set; }
        public string SerialNumber { get; set; }
    }

    #endregion

    #region Terminaltransid
    public class Terminaltransid
    {
        public int? CountryCode { get; set; }
        public string MerchantID { get; set; }
        public string TerminalID { get; set; }
        [JsonIgnore]
        public const string TIMESTAMP_FORMAT = "yyMMddHHmmss";
        public string TimeStamp { get; set; }
        public int? TransNumber { get; set; }
        public bool RepeatIndicator { get; set; }

        #region GettersForProtectNulls  
        public int GetCountryCode()
        {
            return CountryCode ?? -1;
        }
        public string GetMerchantID()
        {
            return MerchantID ?? string.Empty;
        }
        public string GetTerminalID()
        {
            return TerminalID ?? string.Empty;
        }

        public string GetTimeStamp()
        {
            return TimeStamp ?? string.Empty;
        }

        public int GetTransNumber()
        {
            return TransNumber ?? -1;
        }
        #endregion


    }

    #endregion

    #region Transactionreference

    public class Transactionreference
    {
        [JsonIgnore]
        public const string TIMESTAMP_FORMAT = "yyMMddHHmmss";
        public string TimeStamp { get; set; }
        public int? TransNumber { get; set; }
    }

    #endregion

    #region Transactionreferenceresult
    public class Transactionreferenceresult
    {
        public int? MessageType { get; set; }
        public string TimeStamp { get; set; }
        public int? TransNumber { get; set; }
        public string ServicePointData { get; set; }
        public int? FunctionCode { get; set; }
        public string TransactionClass { get; set; }
        public string TransactionID { get; set; }
    }

    #endregion

    #region SecureData3D
    /// <summary>
    /// 3DS verification data used in ecommerce transactions.
    /// According to the card type contains the verification data related to the 3DS procedure.
    /// Should be present when CardDataCaptureMethod indicates manual entry with no POS and CardholderAuthMethod is 3DSecure
    /// </summary>
    public class SecureData3D
    {
        /// <summary>
        /// Identify the card brand.
        /// VISA	Verified by VISA
        /// MASTERCARD Mastercard SecureCode
        /// </summary>
        public string Type;

        /// <summary>
        /// Electronic commerce indicator
        /// Indicates the level of security used when the cardholder provided payment information to the merchant.
        /// </summary>
        public string ECI { get; set; }

        /// <summary>
        /// Hexa String
        /// Transaction identifier
        /// Unique tracking number set by the merchant
        /// </summary>
        public string XID { get; set; }

        /// <summary>
        /// Contains the authentication verification data
        /// Depending on the card type
        /// </summary>
        public AVV AVV { get; set; }
    }

    public class AVV
    {
        /// <summary>
        /// Contains the authentication verification data used by VISA (Verified by Visa)
        /// Will be present when type indicates VISA (Hexa String)
        /// </summary>
        public string CAVV;

        /// <summary>
        /// Contains the authentication verification data used by MASTERCARD (Mastercard SecureCode)
        /// Will be present when type indicates MASTERCARD 24 bytes encoded in Base64
        /// </summary>
        public string UCAF;
    }
    #endregion

    #region IdentificationData
    /// <summary>
    ///Specific data related to ecommerce transaction identification/qualification
    ///TR-REQ Will be present in transaction class 01 and 02
    ///Will contain data to identify/qualify the given transaction from the source
    /// </summary>
    public class IdentificationData
    {
        public string PersistentId { get; set; }
        public string OrderId { get; set; }
        public string PspIdentifier { get; set; }
        public Flag Flag { get; set; }

    }

    /// <summary>
    /// Additional transaction information
    ///Data to extend ecommerce transaction info
    /// </summary>
    public class Flag
    {
        /// <summary>
        ///Provides information about the origin of the transaction
        ///ecom Ecommerce through website/online payment gateway
        /// moto    Mail order/telephone order
        /// recurring Repeat payment based on card on file
        /// </summary>
        public string EntryMode { get; set; }
        /// <summary>
        ///Identifies the application type issuing the transaction
        ///escrow
        ///chatbot
        ///socialecommerce
        /// </summary>
        public string Channel { get; set; }

        public CredentialOnFile CredentialOnFile { get; set; }

        public string PspTransactionId { get; set; }
    }

    public class CredentialOnFile
    {
        /// <summary>
        /// adhoc recurring installment
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// first subsequente
        /// </summary>
        public string Sequence { get; set; }

        /// <summary>
        /// cardholder merchant scheduled
        /// </summary>
        public string Iniciator { get; set; }
    }

    #endregion

    #region PaymentToken

    /// <summary>
    /// Specific data for digital wallets payments
    /// TR-REQ Will be present in TC 01 TI 00 (sales)
    /// RE-REQ Will be present in TC 01 TI 00 (sales)
    /// Token data from wallets payment systems
    /// This object will be present in virtual POS transactions when PAN is tokenized, that is DPAN is used instead of FPAN.
    /// </summary>
    public class PaymentToken
    {
        /// <summary>
        /// cardholder
        /// Identify the token type
        /// apple-pay Token data for apple pay digital wallet
        /// pay-with-google Token data for google pay digital wallet
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        ///Identifies the application type issuing the transaction
        ///escrow
        ///chatbot
        ///socialecommerce
        /// This field would only appear in the request to the EXIT-POINT
        /// </summary>
        public string Channel { get; set; }

        /// <summary>
        ///Cryptogram to validate tokenised PAN data.
        /// </summary>
        public string Cryptogram { get; set; }

    }
    #endregion
}
