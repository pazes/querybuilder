﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POC_QueryBuilder
{
    public class Policy
    {
        public int IdPolicy { get; set; }

        public string Name { get; set; }

        public string Field { get; set; }

        public bool InRequest { get; set; }
    }
}
